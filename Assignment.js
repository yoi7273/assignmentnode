const Joi = require('joi');
const supabase = require('./client')
module.exports = function (app) {
    app.post('/addVintage', function (req, res) {
        const JoiSchema = Joi.object()
          .keys({           
             year: Joi.string().required(),
             quntity: Joi.string().required(),
             price: Joi.string().required()
          })
          .options({ abortEarly: false });
          const input = req.body;
        //console.log(input);
        const { error, value } = JoiSchema.validate(input);
        if (error == null) {
            addvintage(req,res,input);
      }else{
          res.status(500).json(error);
      }
    });
    //------------------------------------------------
    async function addvintage(req, res, input) {
        const { data, error } = await supabase
          .from('addvinatge')
          .insert([
            input
            ]);
            if(error == null){
                res.status(200).json(data);
            }else{
                //console.log(error);
                res.status(400).json(error);
            } 
    }

        app.get('/getVintage', function (req, res) {
            
            getdata(req, res);
        });
        //----------------------------------------------
        async function getdata(req, res) {
            //console.log(req.body);
            const JoiSchema = Joi.object()
            .keys()
            .options({ abortEarly: false });
        const input = req.query;
            const { error, value } = JoiSchema.validate(input);
            if (error == null) {
            const { data } = await supabase.from('addvinatge').select()
            //console.log(data);
            res.status(200).send(data);
            } else {
            res.status(400).send('failed');
            }
        }
   

        app.post('/deleteVintage', function (req, res) {
            console.log(req.body);
            deletevintage(req, res);
        });
        async function deletevintage(req, res) {
            console.log(req.body);
            const JoiSchema = Joi.object()
                .keys({
                    id: Joi.number().required()                  
                })
                .options({ abortEarly: false });
            const input = req.body;
            console.log(input);
            const { error, value } = JoiSchema.validate(input);
            if (error == null) {
                supabase.from('addvinatge').delete()
                    .eq('id',input.id).then((obj) => {
                        console.log(obj);
                        res.status(200).send('Success');
                    })
            } else {
                console.log(error);
                res.status(400).send(error);
            }
        }

    }