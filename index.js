const express = require('express');
var cors = require('cors');
const Joi = require('joi');
const app = express();
app.use(cors());
const port = 8080;



app.use(express.urlencoded())
app.use(express.json())
app.use(express.static(__dirname + '/public'));

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

require('./Assignment')(app);